FROM python:alpine

RUN pip install fastapi uvicorn
RUN echo -e "from fastapi import FastAPI\napp = FastAPI()\n@app.get('/')\ndef get():\n    return {'msg': 'yey'}" > main.py

CMD ["uvicorn", "main:app"]
